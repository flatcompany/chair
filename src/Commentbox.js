import React from 'react';

const Commentbox = props => {
    return ( 
    <div className= "highlightbox__content">
        <img className= "highlightbox__content-avatar" alt="user avatar" src={props.avatar} />
        <p className= "highlightbox__content-text" > {props.content}</p>
        <p className= "highlightbox__content-date"> {props.date}</p>
    </div>

    );
};

export default Commentbox;