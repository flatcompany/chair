import React from 'react';


const Highlight = props => {
    return (<div className = "highlightbox">
                <img className = "highlightbox__image" alt="highlight" src={props.images} />
                <div className = "highlightbox__content"> {props.children} </div>
            </div>
    );
};

export default Highlight;