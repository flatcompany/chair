import React from 'react';
import ReactDOM from 'react-dom';
import './sass/main.scss';
import faker from 'faker';
import Highlight from './Highlight';
import Commentbox from './Commentbox';

class MyHeader extends React.Component {
    render() {
      return (
        <div className="highlight">
        <Highlight images = {faker.image.city()}>
            <Commentbox
            avatar = {faker.image.avatar()}
            content = {faker.lorem.paragraph()} 
            date = {faker.date.month()}
            />
        </Highlight>
        <Highlight images = {faker.image.city()}>
            <Commentbox
            avatar = {faker.image.avatar()}
            content = {faker.lorem.paragraph()} 
            date = {faker.date.month()}
            />
        </Highlight>
        <Highlight images = {faker.image.city()}>
            <Commentbox
            avatar = {faker.image.avatar()}
            content = {faker.lorem.paragraph()} 
            date = {faker.date.month()}
            />
        </Highlight>
        </div>
      );
     
  }
  }
  
  ReactDOM.render(<MyHeader />, document.getElementById('root'));
